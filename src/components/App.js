import React, { useEffect, useState } from 'react';
import logo from './../assets/images/logo.svg';
import Add from './Add';
import List from './List';

function App() {
	const [currencies, setCurrencies] = useState(['BTC', 'LTC']);
	const [pairs, setPairs] = useState({});

	useEffect(() => {
		currencies.length > 0 ?
			fetch(`https://min-api.cryptocompare.com/data/pricemulti?fsyms=${ currencies.join(',') }&tsyms=EUR`)
				.then(res => res.json())
				.then(pairs => {
					setPairs(pairs)
				})
			:
			setPairs({})
	}, [currencies]);

	const addCurrency = (title) => {
		setCurrencies([title, ...currencies]);
	};

	const removeCurrency = (key) => {
		setCurrencies(currencies.filter(item => item.toUpperCase() !== key))
	};

	return (
		<div className="app">
			<header>
				<div className="wrapper container-fluid">
					<div className="row between-xs">
						<img src={ logo } alt=""/>
					</div>
				</div>
			</header>
			<div className="wrapper container-fluid">
				<div className="row between-xs">
					<div className="col-md-4">
						<div className="app__description">
							<h1 className={ 'app__title' }>
								Now you can track <br/>
								all your cryptos here!
							</h1>
							<p>
								Just enter the <br/>
								cryptocurrency code on the <br/>
								form to the right
							</p>
						</div>
					</div>
					<div className="col-md-4">
						<Add addCurrency={ addCurrency }/>
					</div>
				</div>
				<div className="row">
					<div className="col-md-4">
						<List pairs={ pairs } removeCurrency={ removeCurrency }/>
					</div>
				</div>
			</div>
		</div>
	);
}

export default App;
