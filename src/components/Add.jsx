import React, { useEffect, useState } from 'react';
import * as PropTypes from 'prop-types';

function useInput(defaultValue) {
	const [value, setValue] = useState(defaultValue);

	return {
		bind: {
			value,
			onChange: e => setValue(e.target.value),
		},
		clear: () => setValue(''),
		value: () => value
	}
}


export default function Add(props) {
	const input = useInput('');
	const [list, setList] = useState({});
	const [loading, setLoading] = useState(false);

	const fetchAvailableCurrencies = () => {
		setLoading(true);
		fetch(`https://min-api.cryptocompare.com/data/blockchain/list?api_key=356a3041040920a02bee0f5461d8f2f5c2626a887df4ac46e65107dbe56c9f63`)
			.then(res => res.json())
			.then(currencies => {
				setList(Object.keys(currencies.Data));
				setLoading(false);
			});
	};

	useEffect(() => {
		fetchAvailableCurrencies();
	}, []);


	const handleSubmit = e => {
		e.preventDefault();
		if (list.findIndex(item => item.toLowerCase() === input.value()) === -1) {
			alert('Such cryptocurrency doesn\'t exist');
		} else {
			props.addCurrency(input.value());
		}
		input.clear();
	};


	return (
		<div className="add">
			<form className="add__form" onSubmit={ handleSubmit }>
				<div className="add__form__input">
					<label htmlFor="name">
						cryptocurrency code
					</label>
					<input name='name' autoComplete={ 'off' } type="text" { ...input.bind }/>
				</div>
				<input className="add__form__button" type="submit" value='Add'
				       disabled={ loading || input.value() === '' }/>
			</form>
			<p className="add__note">
				Use of this service is subject to terms and conditions
			</p>
		</div>
	)
}

Add.propTypes = {
	addCurrency: PropTypes.func.isRequired
};