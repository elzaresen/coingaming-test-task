import React from 'react';
import * as PropTypes from 'prop-types';
import icon from '../assets/images/icon.svg';

export default function ListItem(props) {
	return (
		<li className="list__item">
			<img src={ icon } alt=""/>
			<div className="list__item__info">
				<div className="list__item__title">{ props.title }</div>
				<div className="list__item__value">{ props.value } €</div>
			</div>
			<div className="list__item__delete" onClick={ () => props.removeCurrency(props.title) }>
				&times;
			</div>
		</li>
	)
}

ListItem.propTypes = {
	removeCurrency: PropTypes.func.isRequired,
	title: PropTypes.string.isRequired,
	value: PropTypes.number.isRequired
};