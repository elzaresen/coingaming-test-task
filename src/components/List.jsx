import React from 'react';
import * as PropTypes from 'prop-types';
import ListItem from './ListItem';

export default function List(props) {
	return (
		<ul className="list">
			{
				Object.keys(props.pairs).map(key =>
					<ListItem key={ key } title={ key } value={ props.pairs[key]['EUR'] }
					          removeCurrency={ props.removeCurrency }/>
				)
			}
		</ul>
	)
}

List.propTypes = {
	pairs: PropTypes.object.isRequired,
	removeCurrency: PropTypes.func.isRequired
};